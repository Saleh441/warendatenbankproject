﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Warendatenbank.Models;

namespace Warendatenbank.Controllers
{
    public class RechnungensController : Controller
    {
        private readonly WarenDBContext _context;

        public RechnungensController(WarenDBContext context)
        {
            _context = context;
        }

        // GET: Rechnungens
        public async Task<IActionResult> Index()
        {
            return View(await _context.Rechnungen.ToListAsync());
        }

        // GET: Rechnungens/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rechnungen = await _context.Rechnungen
                .SingleOrDefaultAsync(m => m.RechnungNr == id);
            if (rechnungen == null)
            {
                return NotFound();
            }

            return View(rechnungen);
        }

        // GET: Rechnungens/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Rechnungens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RechnungNr,RechnungDatum,KundeNr,AuftragNr,SachbearbeiterNr,Warenwert,MwstProzentsatz,RechnungBetrag,DatumFaellig,DatumMahnung1,DatumMahnung2,ZahlbetragGesamt,Zahldatum,Bezahlt")] Rechnungen rechnungen)
        {
            if (ModelState.IsValid)
            {
                _context.Add(rechnungen);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(rechnungen);
        }

        // GET: Rechnungens/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rechnungen = await _context.Rechnungen.SingleOrDefaultAsync(m => m.RechnungNr == id);
            if (rechnungen == null)
            {
                return NotFound();
            }
            return View(rechnungen);
        }

        // POST: Rechnungens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RechnungNr,RechnungDatum,KundeNr,AuftragNr,SachbearbeiterNr,Warenwert,MwstProzentsatz,RechnungBetrag,DatumFaellig,DatumMahnung1,DatumMahnung2,ZahlbetragGesamt,Zahldatum,Bezahlt")] Rechnungen rechnungen)
        {
            if (id != rechnungen.RechnungNr)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(rechnungen);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RechnungenExists(rechnungen.RechnungNr))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(rechnungen);
        }

        // GET: Rechnungens/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rechnungen = await _context.Rechnungen
                .SingleOrDefaultAsync(m => m.RechnungNr == id);
            if (rechnungen == null)
            {
                return NotFound();
            }

            return View(rechnungen);
        }

        // POST: Rechnungens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rechnungen = await _context.Rechnungen.SingleOrDefaultAsync(m => m.RechnungNr == id);
            _context.Rechnungen.Remove(rechnungen);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RechnungenExists(int id)
        {
            return _context.Rechnungen.Any(e => e.RechnungNr == id);
        }
    }
}
