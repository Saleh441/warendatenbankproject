﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Warendatenbank.Models;

namespace Warendatenbank.Controllers
{
    public class KundensController : Controller
    {
        private readonly WarenDBContext _context;

        public KundensController(WarenDBContext context)
        {
            _context = context;
        }

        // GET: Kundens
        public IActionResult Index()
        {            
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> KundenSearch(string SearchKunden)
        {
            
            var x = from k in _context.Kunden where k.Name.Contains(SearchKunden) select k;
            return PartialView("_kunden",x);
        }

        // GET: Kundens/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kunden = await _context.Kunden
                .SingleOrDefaultAsync(m => m.KundeNr == id);
            if (kunden == null)
            {
                return NotFound();
            }

            return View(kunden);
        }

        // GET: Kundens/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Kundens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("KundeNr,Name,Strasse,PlzStrasse,Land,Ort,Kundengruppe,Branche,VertreterNr,GesperrtKz,DatumKreditauskunft,Kreditlimit,SaldoOffeneRechnungen,Jahresumsatz,Vorjahresumsatz")] Kunden kunden)
        {
            if (ModelState.IsValid)
            {
                _context.Add(kunden);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(kunden);
        }

        // GET: Kundens/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kunden = await _context.Kunden.SingleOrDefaultAsync(m => m.KundeNr == id);
            if (kunden == null)
            {
                return NotFound();
            }
            return View(kunden);
        }

        // POST: Kundens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("KundeNr,Name,Strasse,PlzStrasse,Land,Ort,Kundengruppe,Branche,VertreterNr,GesperrtKz,DatumKreditauskunft,Kreditlimit,SaldoOffeneRechnungen,Jahresumsatz,Vorjahresumsatz")] Kunden kunden)
        {
            if (id != kunden.KundeNr)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(kunden);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KundenExists(kunden.KundeNr))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(kunden);
        }

        // GET: Kundens/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kunden = await _context.Kunden
                .SingleOrDefaultAsync(m => m.KundeNr == id);
            if (kunden == null)
            {
                return NotFound();
            }

            return View(kunden);
        }

        // POST: Kundens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var kunden = await _context.Kunden.SingleOrDefaultAsync(m => m.KundeNr == id);
            _context.Kunden.Remove(kunden);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool KundenExists(int id)
        {
            return _context.Kunden.Any(e => e.KundeNr == id);
        }
    }
}
