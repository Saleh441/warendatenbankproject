﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Warendatenbank.Models;

namespace Warendatenbank.Controllers
{
    public class AuftragspositionensController : Controller
    {
        private readonly WarenDBContext _context;

        public AuftragspositionensController(WarenDBContext context)
        {
            _context = context;
        }

        // GET: Auftragspositionens
        public async Task<IActionResult> Index()
        {
            return View(await _context.Auftragspositionen.ToListAsync());
        }

        // GET: Auftragspositionens/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auftragspositionen = await _context.Auftragspositionen
                .SingleOrDefaultAsync(m => m.AuftragNr == id);
            if (auftragspositionen == null)
            {
                return NotFound();
            }

            return View(auftragspositionen);
        }

        // GET: Auftragspositionens/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Auftragspositionens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AuftragNr,PositionNr,PositionAktiv,ArtikelNr,Verkaufsmenge,Verkaufspreis,Termin,Offen,Geliefert,Berechnet")] Auftragspositionen auftragspositionen)
        {
            if (ModelState.IsValid)
            {
                _context.Add(auftragspositionen);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(auftragspositionen);
        }

        // GET: Auftragspositionens/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auftragspositionen = await _context.Auftragspositionen.SingleOrDefaultAsync(m => m.AuftragNr == id);
            if (auftragspositionen == null)
            {
                return NotFound();
            }
            return View(auftragspositionen);
        }

        // POST: Auftragspositionens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AuftragNr,PositionNr,PositionAktiv,ArtikelNr,Verkaufsmenge,Verkaufspreis,Termin,Offen,Geliefert,Berechnet")] Auftragspositionen auftragspositionen)
        {
            if (id != auftragspositionen.AuftragNr)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(auftragspositionen);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AuftragspositionenExists(auftragspositionen.AuftragNr))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(auftragspositionen);
        }

        // GET: Auftragspositionens/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auftragspositionen = await _context.Auftragspositionen
                .SingleOrDefaultAsync(m => m.AuftragNr == id);
            if (auftragspositionen == null)
            {
                return NotFound();
            }

            return View(auftragspositionen);
        }

        // POST: Auftragspositionens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var auftragspositionen = await _context.Auftragspositionen.SingleOrDefaultAsync(m => m.AuftragNr == id);
            _context.Auftragspositionen.Remove(auftragspositionen);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AuftragspositionenExists(int id)
        {
            return _context.Auftragspositionen.Any(e => e.AuftragNr == id);
        }
    }
}
