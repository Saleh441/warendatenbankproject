﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Warendatenbank.Models;

namespace Warendatenbank.Controllers
{
    public class ArtikelsController : Controller
    {
        private readonly WarenDBContext _context;

        public ArtikelsController(WarenDBContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            
            var d = Enum.GetValues(typeof(XXX));
            var b = Enum.GetValues(typeof(YYY));
            ViewData["ArtikelListe"] = new SelectList(d, "Warengruppe");
            ViewData["ArtikelListe2"] = new SelectList(b);
            return View();
        }

        //public async Task<IActionResult> ArtikelCategories1(int SearchArtikel)
        //{
        //    var x = await _context.Artikel.ToListAsync();
        //    ViewData["ArtikelListe"] = new SelectList(x, "Warengruppe");
        //    return View();

        //}


        // GET: Artikels
        public async Task<IActionResult> SearchArtikel(string SearchArtikel)
        {
            var x = from A in await _context.Artikel.ToListAsync() where A.Bezeichnung.Contains(SearchArtikel) select A;
            return PartialView("_SearchArtikel",x);
        }
       
        public async Task<PartialViewResult> ArtikelCategories(string ArtikelListe)
        {
             
             int b = (int)Enum.Parse(typeof(XXX), ArtikelListe);
             var x = from A in await _context.Artikel.ToListAsync() where A.Warengruppe == b select A;
            return PartialView("_SearchArtikel",x);
        }

        public async Task<PartialViewResult> Umsatzstärksten()
        {
            var x = (from A in await _context.Artikel.ToListAsync() orderby A.Jahresumsatz descending select A).Take(5);
            return PartialView("_SearchArtikel", x);
        }

        public async Task<PartialViewResult> AuswahlListe(string ArtikelListe2)
        {
            
            int b = (int)Enum.Parse(typeof(YYY), ArtikelListe2);
            switch (b)
            {                
                case 1:
                   var x = (from A in await _context.Artikel.ToListAsync() orderby A.Jahresumsatz descending select A).Take(5);
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;

            }
            var x = from A in await _context.Artikel.ToListAsync() where A.Warengruppe == b select A;
            return PartialView("_SearchArtikel", x);
        }

        // GET: Artikels/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel
                .SingleOrDefaultAsync(m => m.ArtikelNr == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return View(artikel);
        }

        // GET: Artikels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Artikels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ArtikelNr,Bezeichnung,Warengruppe,Mengeneinheit,Verkaufspreis,Einkaufspreis,Lieferzeit,BestandLager,BestandMinimum,Jahresumsatz,Vorjahresumsatz")] Artikel artikel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(artikel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(artikel);
        }

        // GET: Artikels/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel.SingleOrDefaultAsync(m => m.ArtikelNr == id);
            if (artikel == null)
            {
                return NotFound();
            }
            return View(artikel);
        }

        // POST: Artikels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ArtikelNr,Bezeichnung,Warengruppe,Mengeneinheit,Verkaufspreis,Einkaufspreis,Lieferzeit,BestandLager,BestandMinimum,Jahresumsatz,Vorjahresumsatz")] Artikel artikel)
        {
            if (id != artikel.ArtikelNr)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(artikel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArtikelExists(artikel.ArtikelNr))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(artikel);
        }

        // GET: Artikels/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel
                .SingleOrDefaultAsync(m => m.ArtikelNr == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return View(artikel);
        }

        // POST: Artikels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var artikel = await _context.Artikel.SingleOrDefaultAsync(m => m.ArtikelNr == id);
            _context.Artikel.Remove(artikel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ArtikelExists(string id)
        {
            return _context.Artikel.Any(e => e.ArtikelNr == id);
        }
    }
}
