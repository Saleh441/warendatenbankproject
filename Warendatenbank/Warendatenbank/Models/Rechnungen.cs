﻿using System;
using System.Collections.Generic;

namespace Warendatenbank.Models
{
    public partial class Rechnungen
    {
        public int RechnungNr { get; set; }
        public DateTime? RechnungDatum { get; set; }
        public int KundeNr { get; set; }
        public int? AuftragNr { get; set; }
        public int? SachbearbeiterNr { get; set; }
        public decimal? Warenwert { get; set; }
        public double? MwstProzentsatz { get; set; }
        public decimal? RechnungBetrag { get; set; }
        public DateTime? DatumFaellig { get; set; }
        public DateTime? DatumMahnung1 { get; set; }
        public DateTime? DatumMahnung2 { get; set; }
        public decimal? ZahlbetragGesamt { get; set; }
        public DateTime? Zahldatum { get; set; }
        public decimal? Bezahlt { get; set; }
    }
}
