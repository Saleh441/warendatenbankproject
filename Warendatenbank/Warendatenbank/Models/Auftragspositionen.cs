﻿using System;
using System.Collections.Generic;

namespace Warendatenbank.Models
{
    public partial class Auftragspositionen
    {
        public int AuftragNr { get; set; }
        public int PositionNr { get; set; }
        public decimal? PositionAktiv { get; set; }
        public string ArtikelNr { get; set; }
        public int? Verkaufsmenge { get; set; }
        public decimal? Verkaufspreis { get; set; }
        public DateTime? Termin { get; set; }
        public int? Offen { get; set; }
        public int? Geliefert { get; set; }
        public int? Berechnet { get; set; }
    }
}
