﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Warendatenbank.Models
{
    public partial class Artikel
    {
       
        public string ArtikelNr { get; set; }
        public string Bezeichnung { get; set; }
        public int? Warengruppe { get; set; }
        public string Mengeneinheit { get; set; }
        public decimal? Verkaufspreis { get; set; }
        public decimal? Einkaufspreis { get; set; }
        public int? Lieferzeit { get; set; }
        public int? BestandLager { get; set; }
        public int? BestandMinimum { get; set; }
        public decimal? Jahresumsatz { get; set; }
        public decimal? Vorjahresumsatz { get; set; }
    }   
   [FlagsAttribute] 
    public enum XXX
    {
        Monituren = 1,
        Digital_Kamera = 4,
        Scanner = 3,
        Leser_Drucker = 2
    }

    public enum YYY
    {
        Die_fünf_umsatzstärksten_Artikel = 1,
        BestandArtikel_kleiner_als_der_Mindestbestand = 2,
        Bis_heute_Noch_Nicht_Verkaufte_Artikel = 3,
        Kunden_Noch_Nicht_Bezahlte_Rechnungen= 4
    }
}
