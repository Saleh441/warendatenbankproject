﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Warendatenbank.Models
{
    public partial class WarenDBContext : DbContext
    {
        public virtual DbSet<Artikel> Artikel { get; set; }
        public virtual DbSet<Auftragspositionen> Auftragspositionen { get; set; }
        public virtual DbSet<Kunden> Kunden { get; set; }
        public virtual DbSet<Rechnungen> Rechnungen { get; set; }

        public WarenDBContext(DbContextOptions<WarenDBContext> options)
            :base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=WarenDB;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Artikel>(entity =>
            {
                entity.HasKey(e => e.ArtikelNr);

                entity.Property(e => e.ArtikelNr)
                    .HasColumnName("Artikel_nr")
                    .HasMaxLength(27)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BestandLager).HasColumnName("Bestand_lager");

                entity.Property(e => e.BestandMinimum).HasColumnName("Bestand_minimum");

                entity.Property(e => e.Bezeichnung)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Einkaufspreis).HasColumnType("numeric(10, 2)");

                entity.Property(e => e.Jahresumsatz).HasColumnType("numeric(10, 2)");

                entity.Property(e => e.Mengeneinheit)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Verkaufspreis).HasColumnType("numeric(10, 2)");

                entity.Property(e => e.Vorjahresumsatz).HasColumnType("numeric(10, 2)");
            });

            modelBuilder.Entity<Auftragspositionen>(entity =>
            {
                entity.HasKey(e => new { e.AuftragNr, e.PositionNr });

                entity.Property(e => e.AuftragNr).HasColumnName("Auftrag_nr");

                entity.Property(e => e.PositionNr).HasColumnName("Position_nr");

                entity.Property(e => e.ArtikelNr)
                    .IsRequired()
                    .HasColumnName("Artikel_nr")
                    .HasMaxLength(27)
                    .IsUnicode(false);

                entity.Property(e => e.PositionAktiv)
                    .HasColumnName("Position_aktiv")
                    .HasColumnType("numeric(1, 0)");

                entity.Property(e => e.Termin).HasColumnType("datetime");

                entity.Property(e => e.Verkaufspreis).HasColumnType("numeric(10, 2)");
            });

            modelBuilder.Entity<Kunden>(entity =>
            {
                entity.HasKey(e => e.KundeNr);

                entity.Property(e => e.KundeNr)
                    .HasColumnName("Kunde_nr")
                    .ValueGeneratedNever();

                entity.Property(e => e.Branche)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DatumKreditauskunft)
                    .HasColumnName("Datum_kreditauskunft")
                    .HasColumnType("datetime");

                entity.Property(e => e.GesperrtKz).HasColumnName("Gesperrt_kz");

                entity.Property(e => e.Jahresumsatz).HasColumnType("numeric(10, 2)");

                entity.Property(e => e.Kreditlimit).HasColumnType("numeric(10, 2)");

                entity.Property(e => e.Land)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Ort)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PlzStrasse)
                    .HasColumnName("Plz_strasse")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SaldoOffeneRechnungen)
                    .HasColumnName("Saldo_offene_rechnungen")
                    .HasColumnType("numeric(10, 2)");

                entity.Property(e => e.Strasse)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.VertreterNr).HasColumnName("Vertreter_nr");

                entity.Property(e => e.Vorjahresumsatz).HasColumnType("numeric(10, 2)");
            });

            modelBuilder.Entity<Rechnungen>(entity =>
            {
                entity.HasKey(e => e.RechnungNr);

                entity.Property(e => e.RechnungNr)
                    .HasColumnName("Rechnung_nr")
                    .ValueGeneratedNever();

                entity.Property(e => e.AuftragNr).HasColumnName("Auftrag_nr");

                entity.Property(e => e.Bezahlt).HasColumnType("numeric(1, 0)");

                entity.Property(e => e.DatumFaellig)
                    .HasColumnName("Datum_faellig")
                    .HasColumnType("datetime");

                entity.Property(e => e.DatumMahnung1)
                    .HasColumnName("Datum_mahnung_1")
                    .HasColumnType("datetime");

                entity.Property(e => e.DatumMahnung2)
                    .HasColumnName("Datum_mahnung_2")
                    .HasColumnType("datetime");

                entity.Property(e => e.KundeNr).HasColumnName("Kunde_nr");

                entity.Property(e => e.MwstProzentsatz).HasColumnName("Mwst_prozentsatz");

                entity.Property(e => e.RechnungBetrag)
                    .HasColumnName("Rechnung_betrag")
                    .HasColumnType("numeric(10, 2)");

                entity.Property(e => e.RechnungDatum)
                    .HasColumnName("Rechnung_datum")
                    .HasColumnType("datetime");

                entity.Property(e => e.SachbearbeiterNr).HasColumnName("Sachbearbeiter_nr");

                entity.Property(e => e.Warenwert).HasColumnType("numeric(10, 2)");

                entity.Property(e => e.ZahlbetragGesamt)
                    .HasColumnName("Zahlbetrag_gesamt")
                    .HasColumnType("numeric(10, 2)");

                entity.Property(e => e.Zahldatum).HasColumnType("datetime");
            });
        }
    }
}
