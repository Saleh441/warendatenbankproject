﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Warendatenbank.Models
{
    public partial class Kunden
    {
        public int KundeNr { get; set; }
        public string Name { get; set; }
        public string Strasse { get; set; }
        public string PlzStrasse { get; set; }
        public string Land { get; set; }
        public string Ort { get; set; }
        public int? Kundengruppe { get; set; }
        public string Branche { get; set; }
        public int? VertreterNr { get; set; }
        public int GesperrtKz { get; set; }
        public DateTime? DatumKreditauskunft { get; set; }
        public decimal? Kreditlimit { get; set; }
        public decimal? SaldoOffeneRechnungen { get; set; }
        public decimal? Jahresumsatz { get; set; }
        public decimal? Vorjahresumsatz { get; set; }
    }

}
