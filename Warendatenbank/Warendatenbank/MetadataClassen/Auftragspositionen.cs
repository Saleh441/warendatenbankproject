﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Warendatenbank.MetadataClassen
{
    [ModelMetadataType(typeof(Auftragspositionen.Metadata))]
    public partial class Auftragspositionen
    {
        private sealed class Metadata
        {
            public int AuftragNr { get; set; }
            public int PositionNr { get; set; }
            public decimal? PositionAktiv { get; set; }
            public string ArtikelNr { get; set; }
            public int? Verkaufsmenge { get; set; }
            public decimal? Verkaufspreis { get; set; }
            public DateTime? Termin { get; set; }
            public int? Offen { get; set; }
            public int? Geliefert { get; set; }
            public int? Berechnet { get; set; }
        }

    }
}
