﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Warendatenbank.Models.MetadataClassen
{
    [ModelMetadataType(typeof(Kunden.Metadata))]
    public partial class Kunden
    {
        private sealed class Metadata
        {
            [Display(Name = "Kunden-Nr.:")]
            public int KundeNr { get; set; }
            [Display(Name = "Name der Kunde: ")]
            public string Name { get; set; }
            public string Strasse { get; set; }
            public string PlzStrasse { get; set; }
            public string Land { get; set; }
            public string Ort { get; set; }            
            public int? Kundengruppe { get; set; }
            [ScaffoldColumn(false)]
            public string Branche { get; set; }
            [ScaffoldColumn(false)]
            public int? VertreterNr { get; set; }
            [ScaffoldColumn(false)]
            public int GesperrtKz { get; set; }
            public DateTime? DatumKreditauskunft { get; set; }
            [ScaffoldColumn(false)]
            public decimal? Kreditlimit { get; set; }
            public decimal? SaldoOffeneRechnungen { get; set; }
            public decimal? Jahresumsatz { get; set; }
            [ScaffoldColumn(false)]
            public decimal? Vorjahresumsatz { get; set; }
        }
    }
}
