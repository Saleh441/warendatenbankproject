﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Warendatenbank.Models.MetadataClassen
{
    [ModelMetadataType(typeof(Artikel.Metadata))]
    public partial class Artikel
    {
        private sealed class Metadata
        {
            [Key]
            public string ArtikelNr { get; set; }
            [Display(Name = "Artikel-Name: ")]
            public string Bezeichnung { get; set; }
            [ScaffoldColumn(false)]
            public int? Warengruppe { get; set; }
            [ScaffoldColumn(false)]
            public string Mengeneinheit { get; set; }
            public decimal? Verkaufspreis { get; set; }
            public decimal? Einkaufspreis { get; set; }
            public int? Lieferzeit { get; set; }
            public int? BestandLager { get; set; }
            [ScaffoldColumn(false)]
            public int? BestandMinimum { get; set; }
            public decimal? Jahresumsatz { get; set; }
            [ScaffoldColumn(false)]
            public decimal? Vorjahresumsatz { get; set; }
        }
    }
}
