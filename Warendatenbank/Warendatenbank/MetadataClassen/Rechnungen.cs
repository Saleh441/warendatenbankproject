﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Warendatenbank.MetadataClassen
{
    [ModelMetadataType(typeof(Rechnungen.Metadata))]
    public partial class Rechnungen
    {
        private sealed class Metadata
        {
            [Display(Name ="RechnungsNr.:")]
            public int RechnungNr { get; set; }
            [Display(Name ="RechnungsDatum:")]
            public DateTime? RechnungDatum { get; set; }
            public int KundeNr { get; set; }
            public int? AuftragNr { get; set; }
            [ScaffoldColumn(false)]
            public int? SachbearbeiterNr { get; set; }
            [ScaffoldColumn(false)]
            public decimal? Warenwert { get; set; }           
            public double? MwstProzentsatz { get; set; }
            public decimal? RechnungBetrag { get; set; }
            public DateTime? DatumFaellig { get; set; }
            [ScaffoldColumn(false)]
            public DateTime? DatumMahnung1 { get; set; }
            [ScaffoldColumn(false)]
            public DateTime? DatumMahnung2 { get; set; }
            [ScaffoldColumn(false)]
            public decimal? ZahlbetragGesamt { get; set; }
            public DateTime? Zahldatum { get; set; }
            public decimal? Bezahlt { get; set; }
        }
    }
}
